#!/bin/bash
sudo yum install git -y
git clone https://github.com/deric/docker-nodejs-helloworld.git
cd docker-nodejs-helloworld/
curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -
sudo yum install nodejs -y
npm install express
node index.js