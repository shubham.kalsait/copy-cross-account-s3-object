import boto3
import json

def lambda_handler(event, context):
    source_bucket_name = 'source-bux'
    destination_bucket_name = 'destination-bux'
    s3 = boto3.client('s3')
    jsonObject = s3.get_object(Bucket='destination-bux', Key='customerListInS3.json')
    content = jsonObject['Body']
    jsonObject = json.loads(content.read())
    
    print ('MyJSONOBJECT:')
    print (jsonObject)
    # list of source objects
    #sourcePathList = jsonObject['source_objects']
    sourcePathList = []
    # list of destination objects
    #destinationPathList = jsonObject['destination_objects']
    destinationPathList = []
    for customerData in jsonObject['customer']:
        sourcePathList.append(customerData['src'])
        destinationPathList.append(customerData['dest'])
    
    print ('MysourcePathListBJECT:')
    print (sourcePathList)
    
    print ('MydestinationPathList:')
    print (destinationPathList)
    
    # creating s3-bucket objects
    s3 = boto3.resource('s3')
    sourceBucket = s3.Bucket(source_bucket_name)
    destinationBucket = s3.Bucket(destination_bucket_name)
    
    # get prefix from list
    k=0
    for sourcePath in sourcePathList:
        old_prefix = sourcePath+'/'
        destinationPath = destinationPathList[k]+'/'
        k= k+1
        print('inside 1st for-loop',sourcePath)
        for sourceKey in sourceBucket.objects.filter(Prefix=old_prefix):
            print('inside INNER for-loop')
            sourceBucketKey = { 'Bucket': source_bucket_name, 'Key': sourceKey.key}
            print(sourceBucketKey)
            # replace the prefix
            destinationKey = sourceKey.key.replace(old_prefix, destinationPath, 1)
            print(destinationKey)
            destinationObject = destinationBucket.Object(destinationKey)
            destinationObject.copy(sourceBucketKey)  
